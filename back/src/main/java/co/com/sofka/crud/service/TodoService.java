package co.com.sofka.crud.service;

import co.com.sofka.crud.entity.Todo;

public interface TodoService {
    Iterable<Todo> list();

    Todo save(Todo todo);

    void delete(Long id);

    Todo get(Long id);
}